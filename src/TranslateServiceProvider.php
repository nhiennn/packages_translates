<?php

namespace Gralias\Translates;

use Illuminate\Support\ServiceProvider;

class TranslateServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views','translates');

        $this->publishes([
            __DIR__.'/../migrations' => database_path('migrations')
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../middleware' => app_path('Http/Middleware')
        ], 'middleware');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
    }
}
