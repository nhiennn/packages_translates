<?php
$count = count($data);
$i = 0;
?>
<?= '<?php' . PHP_EOL ?>
<?= '   return [' . PHP_EOL ?>
<?php foreach($data as $k => $v): ?>
<?php
    $i++;
    $comma  = ($i == $count) ? '' : ',';
?>
<?= "       \"$k\" => \"$v\"".$comma  .PHP_EOL ?>
<?php endforeach; ?>
<?= '   ];' . PHP_EOL ?>
<?= '?>' ?>
