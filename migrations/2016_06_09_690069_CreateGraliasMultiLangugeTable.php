<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraliasMultiLangugeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gralias_multi_languages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key');
            $table->string('eng');
            $table->string('vie');
            $table->string('jpn');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gralias_multi_language');
    }
}
